﻿using AIS.MiddleWare.GUI.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;

namespace AIS.MiddleWare.GUI.BlueTooth
{
    internal class Association
    {
        internal static List<string> addDevice()
        {
            try
            {
                Process p = Process.Start(MyDictionary.WindowsDevicePairWizard);
                while (p.HasExited) ;

                return AllComPorts();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        internal static List<string> AllComPorts()
        {
            try
            {
                List<string> res = new List<string>();
                foreach (string s in SerialPort.GetPortNames())
                {
                    res.Add(s);
                }
                if (res.Count == 0) res.Add("No Ports!");
                return res;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
