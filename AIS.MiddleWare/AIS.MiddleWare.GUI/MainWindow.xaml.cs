﻿using AIS.MiddleWare.GUI.BlueTooth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using AIS.MiddleWare.GUI.Common;

namespace AIS.MiddleWare.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string GaragePath = @"C:\AIS-IOT\AIS.MiddleWare.Process.Garage.exe";
        private const string HousePath = @"C:\AIS-IOT\AIS.MiddleWare.Process.House.exe";
        private const string CoolerPath = @"C:\AIS-IOT\AIS.MiddleWare.Process.Cooler.exe";
        private const string FreezerPath = @"C:\AIS-IOT\AIS.MiddleWare.Process.Freezer.exe";

        Process Garage = null;
        Process House = null;
        Process Cooler = null;
        Process Freezer = null;
        public MainWindow()
        {
            InitializeComponent();
            deviceCB.ItemsSource = Enum.GetValues(typeof(MyTypes));
            deviceCB.SelectedIndex = 0;
            bluetoothCB.ItemsSource = Association.AllComPorts();
            bluetoothCB.SelectedIndex = 0;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Garage != null && !Garage.HasExited) Garage.Kill();
            if (House != null && !House.HasExited) House.Kill();
            if (Cooler != null && !Cooler.HasExited) Cooler.Kill();
            if (Freezer != null && !Freezer.HasExited) Freezer.Kill();
        }

        private void AddBluetooth_Click(object sender, RoutedEventArgs e)
        {
            bluetoothCB.ItemsSource = Association.addDevice();
        }

        private void ConnectDB_Click(object sender, RoutedEventArgs e)
        {
            string name = NameTb.Text;
            string bluetoothPort = bluetoothCB.SelectedValue.ToString();
            string deviceType = deviceCB.SelectedValue.ToString();
            if (string.IsNullOrEmpty(name)) name = "Default";
            string arguments = bluetoothPort + " " + name;

            if (MyTypes.House.ToString() == deviceType)
            {
                if (House == null || House.HasExited)
                {
                    rtb.AppendText("\nConnected on Port: " + bluetoothPort + " type: " + deviceType + " with the name: " + name);
                    House = Process.Start(HousePath, arguments);
                }
                else
                {
                    rtb.AppendText("\nAlready one House");
                }
            }
            else
            {
                if (MyTypes.Garage.ToString() == deviceType)
                {
                    if (Garage == null || Garage.HasExited)
                    {
                        rtb.AppendText("\nConnected on Port: " + bluetoothPort + " type: " + deviceType + " with the name: " + name);
                        Garage = Process.Start(GaragePath, arguments);
                    }
                    else
                    {
                        rtb.AppendText("\nAlready one Garage");
                    }
                }

                else
                {
                    if (MyTypes.Cooler.ToString() == deviceType)
                    {
                        if (Cooler == null || Cooler.HasExited)
                        {
                            rtb.AppendText("\nConnected on Port: " + bluetoothPort + " type: " + deviceType + " with the name: " + name);
                            Cooler = Process.Start(CoolerPath, arguments);
                        }
                        else
                        {
                            rtb.AppendText("\nAlready one Cooler");
                        }
                    }
                    else
                    {
                        if (MyTypes.Freezer.ToString() == deviceType)
                        {
                            if (Freezer == null || Freezer.HasExited)
                            {
                                rtb.AppendText("\nConnected on Port: " + bluetoothPort + " type: " + deviceType + " with the name: " + name);
                                Freezer = Process.Start(FreezerPath, arguments);
                            }
                            else
                            {
                                rtb.AppendText("\nAlready one Freezer");
                            }
                        }
                    }
                }
            }
        }

    }
}
