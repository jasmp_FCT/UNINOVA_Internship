﻿using AIS.MiddleWare.Components.Cooler;
using AIS.MiddleWare.Model;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Process.Cooler
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                /*args = new string[3];
                args[1] = "merda";
                args[0] = "com10";*/
                Console.WriteLine("Name: {0} Bluetooth Port: {1}", args[1], args[0]);

                CoolerBrick coolerBrick = new CoolerBrick(args[0], new BrickModel
                {
                    ID = -1,
                    Name = args[1],
                    Motor1 = new DeviceModel { DataName = DataName.Velocidade, DataType = TypeEnumName.@int, Value = "0", Name = "Motor AC", SensorType = MySensorType.Motor },
                    Sensor1 = new DeviceModel { DataName = DataName.Temperatura, DataType = TypeEnumName.@decimal, Value = "0", Name = "Sensor Temperatura", SensorType = MySensorType.Temperature },
                });

                coolerBrick.CoolerWork();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR : " + ex.Message);
                Console.WriteLine("Press any Button to Exit!");
                Console.ReadKey();
                return;
            }
        }
    }
}