﻿using AIS.MiddleWare.Components.Freezer;
using AIS.MiddleWare.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Process.Freezer
{
    class Program
    {
        static void Main(string[] args)
        {
           /*args = new string[3];
args[1] = "merda";
args[0] = "com12";*/
            Console.WriteLine("Name: {0} Bluetooth Port: {1}", args[1], args[0]);
            FreezerBrick freezerbrick = new FreezerBrick(args[0], new BrickModel
            {
                ID = -1,
                Name = args[1],
                Sensor1 = new DeviceModel { DataName = DataName.CocaCola, DataType = TypeEnumName.@int, Value = "0", Name = "RFID", SensorType = MySensorType.Luz },
                Sensor2 = new DeviceModel { DataName = DataName.Pressao, DataType = TypeEnumName.@int, Value = "0", Name = "TOUCH", SensorType = MySensorType.Touch }
            });

            freezerbrick.FreezerWork();
        }
    }
}
