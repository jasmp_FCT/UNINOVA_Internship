﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace AIS.MiddleWare.Components
{
    public class AISHttpApiClient
    {
        private HttpClient client = null;
        public HttpClient Client
        {
            get
            {
                return this.client;
            }
        }
        public void BuildHttpClient(string BaseAddress)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(BaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}
