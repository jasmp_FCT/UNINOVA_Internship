﻿
using AIS.MiddleWare.Model;
using MonoBrick.NXT;
using System;

namespace AIS.MiddleWare.Components
{
    public class AISMiddleWare
    {
        private Brick<Sensor, Sensor, Sensor, Sensor> nxtBrick;
        public Brick<Sensor, Sensor, Sensor, Sensor> NxtBrick
        {
            get
            {
                return nxtBrick;
            }
        }

        private I2CSensor TemperatureSensor = new I2CSensor(I2CMode.LowSpeed, 0x98);
        public byte GetTemperature()
        {
            return TemperatureSensor.ReadRegister(0x00, 1)[0];
        }
        public bool DisconnectBrick()
        {
            try
            {
                nxtBrick.Connection.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool ConnectBrick(string ComPort)
        {
            try
            {
                nxtBrick = new Brick<Sensor, Sensor, Sensor, Sensor>(ComPort);
                nxtBrick.Connection.Open();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void AssociateSensors(int position, MySensorType type)
        {
            switch (position) 
            {
                case 1:
                    switch (type)
                    {
                        case MySensorType.None: nxtBrick.Sensor1 = new NoSensor(); break;
                        case MySensorType.Touch: nxtBrick.Sensor1 = new TouchSensor(); break;
                        case MySensorType.Temperature: nxtBrick.Sensor1 = TemperatureSensor; break;
                        case MySensorType.Sonar: nxtBrick.Sensor1 = new Sonar(); break;
                    }
                    break;
                case 2:
                    switch (type)
                    {
                        case MySensorType.None: nxtBrick.Sensor2 = new NoSensor(); break;
                        case MySensorType.Touch: nxtBrick.Sensor2 = new TouchSensor(); break;
                        case MySensorType.Temperature: nxtBrick.Sensor2 = TemperatureSensor; break;
                        case MySensorType.Sonar: nxtBrick.Sensor2 = new Sonar(); break;
                    }
                    break;
                case 3:
                    switch (type)
                    {
                        case MySensorType.None: nxtBrick.Sensor3 = new NoSensor(); break;
                        case MySensorType.Touch: nxtBrick.Sensor3 = new TouchSensor(); break;
                        case MySensorType.Temperature: nxtBrick.Sensor3 = TemperatureSensor; break;
                        case MySensorType.Sonar: nxtBrick.Sensor3 = new Sonar(); break;
                    }
                    break;
                case 4:
                    switch (type)
                    {
                        case MySensorType.None: nxtBrick.Sensor3 = new NoSensor(); break;
                        case MySensorType.Touch: nxtBrick.Sensor3 = new TouchSensor(); break;
                        case MySensorType.Temperature: nxtBrick.Sensor3 = TemperatureSensor; break;
                        case MySensorType.Sonar: nxtBrick.Sensor3 = new Sonar(); break;
                    }
                    break;
            }
        }

    }
}
