﻿using AIS.MiddleWare.Common;
using AIS.MiddleWare.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Components.Cooler
{
    public class CoolerAPI : AISHttpApiClient
    {
        #region APIStrings
        private const string CoolerAPIString = "Cooler/";

        private const string RegisterCooler = "RegisterCooler?";
        private const string AssociateCoolerToHouse = "AssociateCoolerToHouse?";
        private const string someoneHome = "someoneHome?";
        private const string coolerPower = "coolerPower?";
        private const string Status = "Status?";
        private const string CheckHouseGarageAssociated = "CheckHouseGarageAssociated?";
        private const string CreateAndAssociateSensor = "CreateAndAssociateSensor?";
        private const string CreateAndAssociateActuator = "CreateAndAssociateActuator?";
        private const string AddSensorReading = "AddSensorReading?";
        private const string AddActuatorAction = "AddActuatorAction?";
        #endregion

        public CoolerAPI(string BaseAddress = (ApiStrings.Localhost + CoolerAPIString))
        {
            BuildHttpClient(BaseAddress);
        }

        #region API Methods
        public async Task<long> PostRegisterAsync(BrickModel myCooler)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "Name=" + myCooler.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(RegisterCooler + toAppend, myCooler.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<long>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PutAssociateAsync(BrickModel myCooler)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID;

            HttpResponseMessage response = await Client.PutAsJsonAsync(AssociateCoolerToHouse + toAppend, myCooler.ID.ToString());

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: return false;
                case HttpStatusCode.BadRequest: return false;
                case HttpStatusCode.InternalServerError: return false;
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetsomeoneHomeAsync(BrickModel myCooler)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID;

            HttpResponseMessage response = await Client.GetAsync(someoneHome + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetCheckHouseGarageAssociatedAsync(BrickModel myCooler)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID;

            HttpResponseMessage response = await Client.GetAsync(CheckHouseGarageAssociated + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<int> GetcoolerPowerAsync(BrickModel myCooler, decimal currentTemperature)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID + "&currentTemperature=" + currentTemperature.ToString();

            HttpResponseMessage response = await Client.GetAsync(coolerPower + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<int>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetStatusAsync(BrickModel myCooler)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID;

            HttpResponseMessage response = await Client.GetAsync(Status + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PutStatusAsync(BrickModel myCooler, bool value)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID + "&value=" + value.ToString();

            HttpResponseMessage response = await Client.PutAsJsonAsync(Status + toAppend, myCooler.ID.ToString() + " " + value.ToString());

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostCreateAndAssociateSensorAsync(BrickModel myCooler, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID + "&SensorName=" + myDevice.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(CreateAndAssociateSensor + toAppend, myCooler.ID.ToString() + " " + myDevice.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostCreateAndAssociateActuatorAsync(BrickModel myCooler, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID + "&SensorName=" + myDevice.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(CreateAndAssociateActuator + toAppend, myCooler.ID.ToString() + " " + myDevice.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostAddSensorReadingAsync(BrickModel myCooler, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID + "&SensorName=" + myDevice.Name + "&DataName=" + myDevice.DataName + "&Value=" + myDevice.Value + "&TypeObject=" + myDevice.DataType;

            HttpResponseMessage response = await Client.PostAsJsonAsync(AddSensorReading + toAppend, myCooler.Name);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostAddActuatorActionAsync(BrickModel myCooler, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myCooler == null) throw new ArgumentNullException("myCooler");

            string toAppend = "CoolerID=" + myCooler.ID + "&SensorName=" + myDevice.Name + "&DataName=" + myDevice.DataName + "&Value=" + myDevice.Value + "&TypeObject=" + myDevice.DataType;

            HttpResponseMessage response = await Client.PostAsJsonAsync(AddActuatorAction + toAppend, myCooler.Name);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        #endregion
    }
}
