﻿using AIS.MiddleWare.Model;
using System;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Components.Cooler
{
    public class CoolerBrick : AISMiddleWare
    {
        private CoolerAPI _API = null;
        private BrickModel _myCooler = null;
        public CoolerBrick(string ComPort, BrickModel myCooler)
        {
            if (myCooler == null) throw new ArgumentNullException("myGarage");
            this._myCooler = myCooler;
            this._myCooler.ID = -1;
            this._API = new CoolerAPI();
            Console.WriteLine("{0} - Start Config and register.", this._myCooler.Name);
            if (!BuildAndRegisterBrick(ComPort).Result) throw new Exception("Brick Not Connected");
            Console.WriteLine("{0} - End Config and register.", this._myCooler.Name);
        }
        public void CoolerWork()
        {
            Console.WriteLine("{0} - Start Working.", this._myCooler.Name);
            bool firstPrint = true;
            int print = 0, power = 0;
            decimal temp = 0;
            while (!_API.PutAssociateAsync(this._myCooler).Result)
            {
                if (firstPrint)
                {
                    Console.WriteLine("{0} - Trying to Associate", this._myCooler.Name);
                    firstPrint = false;
                }
            };

            Console.WriteLine("{0} - Associated", this._myCooler.Name);
            while (!_API.GetCheckHouseGarageAssociatedAsync(_myCooler).Result) ;
            Console.WriteLine("{0} - House associated to a Garage", this._myCooler.Name);

            while (true)
            {
                if (_API.GetsomeoneHomeAsync(_myCooler).Result)
                {
                    if (print == 1)
                    {
                        Console.WriteLine("{0} - Someone is Home!", this._myCooler.Name);
                    }

                    temp = getTemp().Result;
                    if (temp == -100) temp = 24;
                    power = _API.GetcoolerPowerAsync(_myCooler, temp).Result;
                    while(!TurnOnCooler(power).Result);
                    Console.WriteLine("{0} - Cooler: On Power: {1} Temp: {2}", this._myCooler.Name,power,temp);
                    print = 0;
                }
                else
                {
                    if (_API.GetStatusAsync(_myCooler).Result)
                    {
                        if (print == 0)
                        {
                            Console.WriteLine("{0} - Nobody Home", this._myCooler.Name);
                            while (!TurnOFFCooler().Result) ;
                            Console.WriteLine("{0} - Cooler OFF", this._myCooler.Name);
                            print = 1;
                        }
                    }
                }
            }
        }

        #region Private
        private async Task<bool> BuildAndRegisterBrick(string ComPost)
        {
            try
            {

                bool res = ConnectBrick(ComPost);
                if (!res) return false;

                this._myCooler.ID = await _API.PostRegisterAsync(this._myCooler);
                if (this._myCooler.ID == -1)
                {
                    DisconnectBrick();
                    return false;
                }

                AssociateSensors(1, MySensorType.Temperature);

                Task<bool> res1 = _API.PostCreateAndAssociateActuatorAsync(this._myCooler, this._myCooler.Motor1);
                Task<bool> res2 = _API.PostCreateAndAssociateSensorAsync(this._myCooler, this._myCooler.Sensor1);
                if (!await res1 || !await res2) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private async Task<bool> TurnOnCooler(int power)
        {
            try
            {
                int res = 5 * power;
                if (res > 100)
                {
                    res = 100;
                }

                if (res < -100)
                {
                    res = -100;
                }
                _myCooler.Motor1.Value = res.ToString();

                NxtBrick.MotorA.On((sbyte)res);
                await _API.PutStatusAsync(_myCooler, true);
                return await _API.PostAddActuatorActionAsync(_myCooler, _myCooler.Motor1);
            }
            catch (Exception)
            {
                return false;
            }
        }
        private async Task<bool> TurnOFFCooler()
        {
            try
            {
                _myCooler.Motor1.Value = "0";
                NxtBrick.MotorA.Off();
                bool res1 = await _API.PutStatusAsync(_myCooler, false);
                bool res2 = await _API.PostAddActuatorActionAsync(_myCooler, _myCooler.Motor1);
                return res1 && res2;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private async Task<decimal> getTemp()
        {
            try
            {
                string value = GetTemperature().ToString();
                _myCooler.Sensor1.Value = value;
                await _API.PostAddSensorReadingAsync(_myCooler, _myCooler.Sensor1);
                return decimal.Parse(value);
            }
            catch (Exception)
            {
                return -100;
            }
        }
        #endregion
    }
}
