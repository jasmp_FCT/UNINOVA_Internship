﻿using AIS.MiddleWare.Common;
using AIS.MiddleWare.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace AIS.MiddleWare.Components.Freezer
{
    public class FreezerAPI : AISHttpApiClient
    {
        #region APIStrings
        private const string FreezerNameAPI = "Freezer/";

        private const string RegisterFreezer = "RegisterFreezer?";
        private const string CreateAndAssociateSensor = "CreateAndAssociateSensor?";
        private const string addOrRemoveProduct = "addOrRemovePorduct?";
        private const string getList = "List?";
        #endregion

        public FreezerAPI(string BaseAddress = (ApiStrings.Localhost + FreezerNameAPI))
        {
            BuildHttpClient(BaseAddress);
        }

        #region API Methods
        public async Task<long> PostRegisterAsync(BrickModel myFreezer)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myFreezer == null) throw new ArgumentNullException("myFreezer");
            string toAppend = "Name=" + myFreezer.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(RegisterFreezer + toAppend, myFreezer.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<long>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }

        public async Task<bool> PostCreateAndAssociateSensorAsync(BrickModel myFreezer, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myFreezer == null) throw new ArgumentNullException("myFreezer");

            string toAppend = "FreezerID=" + myFreezer.ID + "&SensorName=" + myDevice.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(CreateAndAssociateSensor + toAppend, myFreezer.ID.ToString() + " " + myDevice.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }

        public async Task<bool> PostaddOrRemoveProductAsync(BrickModel myFreezer, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myFreezer == null) throw new ArgumentNullException("myFreezer");

            string toAppend = "FreezerID=" + myFreezer.ID + "&SensorName=" + myDevice.Name + "&productID=" + myDevice.DataName;

            HttpResponseMessage response = await Client.PostAsJsonAsync(addOrRemoveProduct + toAppend, myFreezer.Name);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }

        public async Task<List<DataModel>> GetList(BrickModel myFreezer, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myFreezer == null) throw new ArgumentNullException("myFreezer");

            string toAppend = "FreezerID=" + myFreezer.ID + "&SensorName=" + myDevice.Name;

            HttpResponseMessage response = await Client.GetAsync(getList + toAppend);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<List<DataModel>>();
                case HttpStatusCode.NotFound: return null;
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        #endregion
    }
}
