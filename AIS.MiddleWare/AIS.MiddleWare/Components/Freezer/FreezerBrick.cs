﻿using AIS.MiddleWare.Model;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Components.Freezer
{
    public class FreezerBrick : AISMiddleWare
    {
        private SerialPort _SP = null;
        private FreezerAPI _API = null;
        private BrickModel _myFreezer = null;

        public FreezerBrick(string serialPort, BrickModel myFreezer)
        {
            if (string.IsNullOrWhiteSpace(serialPort)) throw new ArgumentNullException("serialPort");
            if (myFreezer == null) throw new ArgumentNullException("myFreezer");

            _SP = new SerialPort(serialPort, 38400);
            _myFreezer = myFreezer;
            _API = new FreezerAPI();
            Console.WriteLine("{0} - Start Config and register.", this._myFreezer.Name);
            if (!BuildAndRegisterBrick().Result) throw new Exception("Brick Not Connected");
            Console.WriteLine("{0} - End Config and register.", this._myFreezer.Name);
        }

        private async Task<bool> BuildAndRegisterBrick()
        {
            try
            {

                this._myFreezer.ID = await _API.PostRegisterAsync(this._myFreezer);
                if (this._myFreezer.ID == -1)
                {
                    return false;
                }

                Task<bool> res1 = _API.PostCreateAndAssociateSensorAsync(this._myFreezer, this._myFreezer.Sensor1);
                Task<bool> res2 = _API.PostCreateAndAssociateSensorAsync(this._myFreezer, this._myFreezer.Sensor2);

                if (!await res1 || !await res2) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void FreezerWork()
        {
            try
            {
                _SP.Open();
                Console.WriteLine("Connection Open");
            }
            catch (Exception)
            {
                throw new Exception("Connection Not Open");
            }

            while (_SP.IsOpen)
            {
                string aux = _SP.ReadLine();

                if (aux.IndexOf("?") != -1)
                {
                    switch (aux[5].ToString() + aux[7].ToString())
                    {
                        case "62":
                            {
                                _myFreezer.Sensor1.DataName = DataName.Lipton; //este e mesmo litpon
                                if (_API.PostaddOrRemoveProductAsync(_myFreezer, _myFreezer.Sensor1).Result)
                                {
                                    Console.WriteLine("Product Add :  {0}", DataName.Lipton);
                                }
                                else
                                {
                                    Console.WriteLine("Product Removed :  {0}", DataName.Lipton);
                                }

                                break;
                            }
                        case "dd":
                            {
                                _myFreezer.Sensor1.DataName = DataName.CocaCola;
                                if (_API.PostaddOrRemoveProductAsync(_myFreezer, _myFreezer.Sensor1).Result)
                                {
                                    Console.WriteLine("Product Add :  {0}", DataName.CocaCola);
                                }
                                else
                                {
                                    Console.WriteLine("Product Removed :  {0}", DataName.CocaCola);
                                }
                                break;
                            }
                        case "??":
                            {
                                Console.WriteLine("Console Touch!");
                                IEnumerable<DeviceModelPrint> list = ChangeLanguage(_API.GetList(_myFreezer, _myFreezer.Sensor1).Result);
                                Console.WriteLine("List Of Interactions");
                                Console.WriteLine("Start: ");

                                foreach (DeviceModelPrint a in list)
                                {
                                    Console.WriteLine("{0} at {1} one {2}", a.Value, a.Name, a.Date);
                                }

                                Console.WriteLine("End");
                                Console.WriteLine();
                                Console.WriteLine("Actual : ");

                                var Lipton = list.OrderByDescending(b => b.Date).Where(c => c.Name == DataName.Lipton.ToString()).FirstOrDefault();
                                var cocacola = list.OrderByDescending(b => b.Date).Where(c => c.Name == DataName.CocaCola.ToString()).FirstOrDefault();

                                if (Lipton != null && Lipton.Value != "REMOVED") Console.WriteLine("{0} at {1} one {2}", Lipton.Value, Lipton.Name, Lipton.Date);
                                if (cocacola != null && cocacola.Value != "REMOVED") Console.WriteLine("{0} at {1} one {2}", cocacola.Value, cocacola.Name, cocacola.Date);
                                Console.WriteLine("-----------------------------------------");

                                break;
                            }


                    }
                }
                else
                {
                    Console.WriteLine(aux);
                }
            }

            throw new Exception("Connection Lost");
        }


        private IEnumerable<DeviceModelPrint> ChangeLanguage(List<DataModel> aux)
        {
            List<DeviceModelPrint> print = new List<DeviceModelPrint>();
            DeviceModelPrint lixo;
            foreach (DataModel a in aux)
            {
                lixo = new DeviceModelPrint { Date = a.Date };

                switch (a.value.ToString())
                {
                    case "True": lixo.Value = "ADD"; break;
                    case "False": lixo.Value = "REMOVED"; break;
                }

                switch (a.NameProp)
                {
                    case (int)DataName.CocaCola: lixo.Name = "CocaCola"; break;
                    case (int)DataName.Lipton: lixo.Name = "Lipton"; break;
                }
                print.Add(lixo);
            }
            return print;
        }
    }
}
