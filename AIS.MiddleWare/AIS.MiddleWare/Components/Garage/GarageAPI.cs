﻿using AIS.MiddleWare.Common;
using AIS.MiddleWare.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Components.Garage
{
    public class GarageAPI : AISHttpApiClient
    {
        #region APIStrings
        private const string GarageAPIString = "Garage/";

        private const string RegisterGarage = "RegisterGarage?";
        private const string AssociateGarageToHome = "AssociateGarageToHome?";
        private const string StatusOfGarage = "StatusOfGarage?";
        private const string GarageDoor = "GarageDoor?";
        private const string Request = "Request?";
        private const string CreateAndAssociateSensor = "CreateAndAssociateSensor?";
        private const string CreateAndAssociateActuator = "CreateAndAssociateActuator?";
        private const string AddSensorReading = "AddSensorReading?";
        private const string AddActuatorAction = "AddActuatorAction?";
        private const string CheckFire = "Fire?";
        #endregion

        public GarageAPI(string BaseAddress = (ApiStrings.Localhost + GarageAPIString))
        {
            BuildHttpClient(BaseAddress);
        }

        #region API Methods
        public async Task<long> PostRegisterAsync(BrickModel myGarage)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "Name=" + myGarage.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(RegisterGarage + toAppend, myGarage.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<long>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PutAssociateAsync(BrickModel myGarage)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID;

            HttpResponseMessage response = await Client.PutAsJsonAsync(AssociateGarageToHome + toAppend, myGarage.ID.ToString());

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: return false;
                case HttpStatusCode.BadRequest: return false;
                case HttpStatusCode.InternalServerError: return false;
                default: return false;
            }
        }
        public async Task<bool> GetStatusOfGarageAsync(BrickModel myGarage)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID;

            HttpResponseMessage response = await Client.GetAsync(StatusOfGarage + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PutStatusOfGarageAsync(BrickModel myGarage, bool value)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID + "&value=" + value.ToString();

            HttpResponseMessage response = await Client.PutAsJsonAsync(StatusOfGarage + toAppend, myGarage.ID.ToString() + " " + value.ToString());

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetGarageDoorAsync(BrickModel myGarage)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID;

            HttpResponseMessage response = await Client.GetAsync(GarageDoor + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PutGarageDoorAsync(BrickModel myGarage, bool value)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID + "&value=" + value.ToString();

            HttpResponseMessage response = await Client.PutAsJsonAsync(GarageDoor + toAppend, myGarage.ID.ToString() + " " + value.ToString());

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetRequestAsync(BrickModel myGarage)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID;

            HttpResponseMessage response = await Client.GetAsync(Request + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PutRequestAsync(BrickModel myGarage, bool value)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID + "&value=" + value.ToString();

            HttpResponseMessage response = await Client.PutAsJsonAsync(Request + toAppend, myGarage.ID.ToString() + " " + value.ToString());

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetFireAsync(BrickModel myGarage)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID;

            HttpResponseMessage response = await Client.GetAsync(CheckFire + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostCreateAndAssociateSensorAsync(BrickModel myGarage, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID + "&SensorName=" + myDevice.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(CreateAndAssociateSensor + toAppend, myGarage.ID.ToString() + " " + myDevice.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostCreateAndAssociateActuatorAsync(BrickModel myGarage, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID + "&SensorName=" + myDevice.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(CreateAndAssociateActuator + toAppend, myGarage.ID.ToString() + " " + myDevice.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostAddSensorReadingAsync(BrickModel myGarage, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID + "&SensorName=" + myDevice.Name + "&DataName=" + myDevice.DataName + "&Value=" + myDevice.Value + "&TypeObject=" + myDevice.DataType;

            HttpResponseMessage response = await Client.PostAsJsonAsync(AddSensorReading + toAppend, myGarage.Name);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostAddActuatorActionAsync(BrickModel myGarage, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myGarage == null) throw new ArgumentNullException("myGarage");

            string toAppend = "GarageID=" + myGarage.ID + "&SensorName=" + myDevice.Name + "&DataName=" + myDevice.DataName + "&Value=" + myDevice.Value + "&TypeObject=" + myDevice.DataType;

            HttpResponseMessage response = await Client.PostAsJsonAsync(AddActuatorAction + toAppend, myGarage.Name);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        #endregion


    }
}
