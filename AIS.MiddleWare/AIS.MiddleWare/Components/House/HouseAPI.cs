﻿using AIS.MiddleWare.Common;
using AIS.MiddleWare.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Components.House
{
    public class HouseAPI : AISHttpApiClient
    {
        #region APIStrings
        private const string HouseAPIString = "House/";

        private const string RegisterHouse = "RegisterHouse?";
        private const string CreateAndAssociateSensor = "CreateAndAssociateSensor?";
        private const string CreateAndAssociateActuator = "CreateAndAssociateActuator?";
        private const string MaxTemperature = "MaxTemperature?";
        private const string MinTemperature = "MinTemperature?";
        private const string SomeOneInHome = "SomeOneInHome?";
        private const string AddSensorReading = "AddSensorReading?";
        private const string HaveGarage = "HaveGarage?";
        private const string AddActuatorAction = "AddActuatorAction?";
        private const string Fire = "Fire?";
        #endregion

        public HouseAPI(string BaseAddress = (ApiStrings.Localhost + HouseAPIString))
        {
            BuildHttpClient(BaseAddress);
        }

        #region API Methods
        public async Task<long> PostRegisterAsync(BrickModel myHouse)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myCooler");
            string toAppend = "Name=" + myHouse.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(RegisterHouse + toAppend, myHouse.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<long>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetMaxTemperatureAsync(BrickModel myHouse)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID;

            HttpResponseMessage response = await Client.GetAsync(MaxTemperature + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PutMaxTemperatureAsync(BrickModel myHouse, bool value)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID + "&value=" + value.ToString();

            HttpResponseMessage response = await Client.PutAsJsonAsync(MaxTemperature + toAppend, myHouse.ID.ToString() + " " + value.ToString());

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetMinTemperatureAsync(BrickModel myHouse)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID;

            HttpResponseMessage response = await Client.GetAsync(MinTemperature + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PutMinTemperatureAsync(BrickModel myHouse, bool value)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID + "&value=" + value.ToString();

            HttpResponseMessage response = await Client.PutAsJsonAsync(MinTemperature + toAppend, myHouse.ID.ToString() + " " + value.ToString());

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetHaveGarageAsync(BrickModel myHouse)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID;

            HttpResponseMessage response = await Client.GetAsync(HaveGarage + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetFire(BrickModel myHouse)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID;

            HttpResponseMessage response = await Client.GetAsync(Fire + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PutFire(BrickModel myHouse, bool value)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID + "&value=" + value.ToString();

            HttpResponseMessage response = await Client.PutAsJsonAsync(Fire + toAppend, myHouse.ID.ToString() + " " + value.ToString());

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> GetSomeOneInHomeAsync(BrickModel myHouse)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID;

            HttpResponseMessage response = await Client.GetAsync(SomeOneInHome + toAppend);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostCreateAndAssociateSensorAsync(BrickModel myHouse, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID + "&SensorName=" + myDevice.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(CreateAndAssociateSensor + toAppend, myHouse.ID.ToString() + " " + myDevice.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostCreateAndAssociateActuatorAsync(BrickModel myHouse, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID + "&SensorName=" + myDevice.Name;

            HttpResponseMessage response = await Client.PostAsJsonAsync(CreateAndAssociateActuator + toAppend, myHouse.ID.ToString() + " " + myDevice.Name);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostAddSensorReadingAsync(BrickModel myHouse, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID + "&SensorName=" + myDevice.Name + "&DataName=" + myDevice.DataName + "&Value=" + myDevice.Value + "&TypeObject=" + myDevice.DataType;

            HttpResponseMessage response = await Client.PostAsJsonAsync(AddSensorReading + toAppend, myHouse.Name);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        public async Task<bool> PostAddActuatorActionAsync(BrickModel myHouse, DeviceModel myDevice)
        {
            if (Client == null) throw new NullReferenceException("Client Not Initialize");
            if (myHouse == null) throw new ArgumentNullException("myHouse");

            string toAppend = "HouseID=" + myHouse.ID + "&SensorName=" + myDevice.Name + "&DataName=" + myDevice.DataName + "&Value=" + myDevice.Value + "&TypeObject=" + myDevice.DataType;

            HttpResponseMessage response = await Client.PostAsJsonAsync(AddActuatorAction + toAppend, myHouse.Name);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK: return await response.Content.ReadAsAsync<bool>();
                case HttpStatusCode.NotFound: throw new FormatException(HttpStatusCode.NotFound.ToString());
                case HttpStatusCode.BadRequest: throw new FormatException(HttpStatusCode.BadRequest.ToString());
                case HttpStatusCode.InternalServerError: throw new FormatException(HttpStatusCode.InternalServerError.ToString());
                default: throw new NotImplementedException("Status Code Not Implemented");
            }
        }
        #endregion
    }
}
