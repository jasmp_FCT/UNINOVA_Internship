﻿using AIS.MiddleWare.Model;
using System;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Components.House
{
    public class HouseBrick : AISMiddleWare
    {
        private HouseAPI _API = null;
        private BrickModel _myHouse = null;

        public HouseBrick(string ComPort, BrickModel myHouse)
        {
            if (myHouse == null) throw new ArgumentNullException("myHouse");
            this._myHouse = myHouse;
            this._myHouse.ID = -1;
            this._API = new HouseAPI();
            Console.WriteLine("{0} - Start Config and register.", this._myHouse.Name);
            if (!BuildAndRegisterBrick(ComPort).Result) throw new Exception("Brick Not Connected");
            Console.WriteLine("{0} - End Config and register.", this._myHouse.Name);
        }

        public void HouseWork()
        {
            int state = 0;

            Console.WriteLine("{0} - Start Working.", this._myHouse.Name);
            while (!_API.GetHaveGarageAsync(_myHouse).Result) ;
            Console.WriteLine("{0} - Garage Associated.", this._myHouse.Name);

            while (true)
            {

                if (FireONButton().Result)
                {
                    Fire(true);
                }

                if (FireOFFButton().Result)
                {
                    Fire(false);
                }

                if (_API.GetSomeOneInHomeAsync(_myHouse).Result) //Check if car is in garage
                {
                    //someoneisinHome
                    if (state == 1) //so para fazer 1 vez
                    {
                        if (TurnOnPresenceLight().Result)
                        {
                            Console.WriteLine("{0} - Lights ON Someone home.", this._myHouse.Name);
                            state = 0;
                        }
                    }
                }
                else
                {
                    //nobody is Home
                    if (state == 0)  //so para fazer 1 vez
                    {
                        if (TurnOFFPresenceLight().Result)
                        {
                            Console.WriteLine("{0} - Lights OFF Nobody home.", this._myHouse.Name);
                            state = 1;
                        }
                    }

                }
            }
        }

        #region Private
        private async Task<bool> BuildAndRegisterBrick(string ComPost)
        {
            try
            {

                bool res = ConnectBrick(ComPost);
                if (!res) return false;

                this._myHouse.ID = await _API.PostRegisterAsync(this._myHouse);
                if (this._myHouse.ID == -1)
                {
                    DisconnectBrick();
                    return false;
                }

                AssociateSensors(1, MySensorType.Touch);
                AssociateSensors(2, MySensorType.Touch);

                Task<bool> res1 = _API.PostCreateAndAssociateActuatorAsync(this._myHouse, this._myHouse.Motor1);
                Task<bool> res2 = _API.PostCreateAndAssociateActuatorAsync(this._myHouse, this._myHouse.Motor2);
                Task<bool> res3 = _API.PostCreateAndAssociateActuatorAsync(this._myHouse, this._myHouse.Motor3);
                Task<bool> res4 = _API.PostCreateAndAssociateSensorAsync(this._myHouse, this._myHouse.Sensor1);
                Task<bool> res5 = _API.PostCreateAndAssociateSensorAsync(this._myHouse, this._myHouse.Sensor2);

                if (!await res1 || !await res2 || !await res3 || !await res4 || !await res5) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private async Task<bool> TurnOnPresenceLight()
        {
            _myHouse.Motor1.Value = "60";
            _myHouse.Motor2.Value = "0";

            Task<bool> res1 = _API.PostAddActuatorActionAsync(_myHouse, _myHouse.Motor1);
            Task<bool> res2 = _API.PostAddActuatorActionAsync(_myHouse, _myHouse.Motor2);

            NxtBrick.MotorA.On(60);
            NxtBrick.MotorB.Off();

            return await res1 && await res2;
        }
        private async Task<bool> TurnOFFPresenceLight()
        {
            _myHouse.Motor1.Value = "0";
            _myHouse.Motor2.Value = "60";

            Task<bool> res1 = _API.PostAddActuatorActionAsync(_myHouse, _myHouse.Motor1);
            Task<bool> res2 = _API.PostAddActuatorActionAsync(_myHouse, _myHouse.Motor2);

            NxtBrick.MotorA.Off();
            NxtBrick.MotorB.On(60);

            return await res1 && await res2;
        }
        private async Task Fire(bool value)
        {
            if (value)
            {
                _myHouse.Motor3.Value = "100";
                NxtBrick.MotorC.On(100);
            }
            else
            {
                _myHouse.Motor3.Value = "0";
                NxtBrick.MotorC.Off();
            }
            await _API.PostAddActuatorActionAsync(_myHouse, _myHouse.Motor3);
            await _API.PutFire(_myHouse, value);
        }
        private async Task<bool> FireONButton()
        {
            string lixo = NxtBrick.Sensor1.ReadAsString();
            bool res = false;

            if (lixo == "0")
            {
                _myHouse.Sensor1.Value = bool.FalseString;
            }
            else
            {
                _myHouse.Sensor1.Value = bool.TrueString;
                res = true;
            }

            await _API.PostAddSensorReadingAsync(_myHouse, _myHouse.Sensor1);
            return res;

        }
        private async Task<bool> FireOFFButton()
        {
            string lixo = NxtBrick.Sensor2.ReadAsString();
            bool res = false;

            if (lixo == "0")
            {
                _myHouse.Sensor2.Value = bool.FalseString;
            }
            else
            {
                _myHouse.Sensor2.Value = bool.TrueString;
                res = true;
            }

            await _API.PostAddSensorReadingAsync(_myHouse, _myHouse.Sensor2);

            return res;
        }
        #endregion
    }
}
