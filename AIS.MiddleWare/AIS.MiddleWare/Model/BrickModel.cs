﻿namespace AIS.MiddleWare.Model
{
    public class BrickModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public DeviceModel Motor1 { get; set; }
        public DeviceModel Motor2 { get; set; }
        public DeviceModel Motor3 { get; set; }
        public DeviceModel Sensor1 { get; set; }
        public DeviceModel Sensor2 { get; set; }
        public DeviceModel Sensor3 { get; set; }
        public DeviceModel Sensor4 { get; set; }
    }
}
