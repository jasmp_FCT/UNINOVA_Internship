﻿using System;
namespace AIS.MiddleWare.Model
{
    public class DataModel
    {
        public long Id { get; set; }
        public int NameProp { get; set; }
        public string value { get; set; }
        public DateTime Date { get; set; }
        public long DeviceId { get; set; }
        public int typeEnum { get; set; }

    }
}
