﻿namespace AIS.MiddleWare.Model
{
    public class DeviceModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public TypeEnumName DataType { get; set; }
        public DataName DataName { get; set; }
        public MySensorType SensorType { get; set; }
    }
}
