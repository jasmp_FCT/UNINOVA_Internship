﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Model
{
    public class DeviceModelPrint
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Value { get; set; }
    }
}
