﻿namespace AIS.MiddleWare.Model
{
    public enum TypeEnumName : int
    {
        @int = 1,
        @decimal = 2,
        @string = 3
    }

    public enum DataName : short
    {
        Velocidade = 1,
        Temperatura = 2,
        Distancia = 3,
        Pressao = 4,
        Lipton = 6,
        CocaCola = 5
    }
    public enum MySensorType
    {
        None = 0,
        Touch = 1,
        Temperature = 2,
        Sonar = 4,
        Motor = 5,
        Luz = 6
    }
}
