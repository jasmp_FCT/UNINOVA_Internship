﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AISCloud.Models.DAL;
using AISCloud.Common;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;

namespace AISCloud.Controllers
{
    public class FreezerController : ApiController
    {
        private AISCloudModelContainer db = new AISCloudModelContainer();

        #region WebAPI

        #region RegisterFreezer POST

        /// <summary>
        /// Register a Freezer
        /// </summary>
        /// <param name="Name">Freezer Name</param>
        /// <returns></returns>
        [ActionName(Dictionary.FreezerWebAPI.RegisterFreezer)]
        [ResponseType(typeof(long))]
        public async Task<IHttpActionResult> PostRegisterFreezer(string Name)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.FREEZER).FirstOrDefault();
            Resource Freezer = db.Resource.Add(new Resource { Name = Name, ResourceTypesId = type.Id });

            if (await db.SaveChangesAsync() == 1) return Ok(Freezer.Id);
            return InternalServerError();
        }
        #endregion

        #region CreateAndAssociateSensor POST

        /// <summary>
        /// Create and associate a Sensor to the Freezer
        /// </summary>
        /// <param name="FreezerID"></param>
        /// <param name="SensorName"></param>
        /// <returns></returns>
        [ActionName(Dictionary.FreezerWebAPI.CreateAndAssociateSensor)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostCreateAndAssociateSensor(long FreezerID, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType type = db.DeviceType.Where(b => b.Name == DevicesTypeName.SENSOR).FirstOrDefault();
            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.FREEZER).FirstOrDefault();
            Resource resource = await db.Resource.FindAsync(FreezerID);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a Freezer or you are not register");

            Device sensor = db.Device.Add(new Device { Name = SensorName, DeviceTypeId = type.Id, ResourceId = FreezerID });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }

        #endregion

        #region addOrRemovePorduct POST

        /// <summary>
        /// Add or remove Product from the freezer
        /// </summary>
        /// <param name="FreezerID"></param>
        /// <param name="SensorName"></param>
        /// <param name="productID">CocaCola or redbull</param>
        /// <returns> true if add false if removed</returns>
        [ActionName(Dictionary.FreezerWebAPI.addOrRemovePorduct)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostaddOrRemovePorduct(long FreezerID, string SensorName, DataName productID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.FREEZER).FirstOrDefault();
            Resource resource = await db.Resource.FindAsync(FreezerID);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a Freezer or you are not register");

            Device dev = db.Device.Where(b => b.ResourceId == resource.Id && b.Name == SensorName).FirstOrDefault();
            if (dev == null) return BadRequest("No Sensor");

            Data aux = db.Data.Where(b => b.NameProp == productID && b.DeviceId == dev.Id).OrderByDescending(b => b.Id).FirstOrDefault();

            if (aux == null)
            {
                db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = productID, typeEnum = TypeEnumName.@int, value = bool.TrueString });
                await db.SaveChangesAsync();
                return Ok(true);
            }

            if (bool.Parse(aux.value))
            {
                db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = productID, typeEnum = TypeEnumName.@int, value = bool.FalseString });
                await db.SaveChangesAsync();
                return Ok(false);
            }
            else
            {
                db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = productID, typeEnum = TypeEnumName.@int, value = bool.TrueString });
                await db.SaveChangesAsync();
                return Ok(true);
            }


        }
        #endregion

        #region getList Get


        /// <summary>
        /// Get List of all reading tags of the freezer
        /// </summary>
        /// <param name="FreezerID"></param>
        /// <param name="SensorName"></param>
        /// <returns></returns>
        [ActionName(Dictionary.FreezerWebAPI.getList)]
        [ResponseType(typeof(List<Data>))]
        public async Task<IHttpActionResult> getList(long FreezerID, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.FREEZER).FirstOrDefault();
            Resource resource = await db.Resource.FindAsync(FreezerID);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a Freezer or you are not register");

            Device dev = db.Device.Where(b => b.ResourceId == resource.Id && b.Name == SensorName).FirstOrDefault();
            if (dev == null) return BadRequest("No Sensor");

            IEnumerable<Data> aux = db.Data.Where(b => b.DeviceId == dev.Id);
            if (aux != null) return Ok(aux);
            return NotFound();

        }

        #endregion

        #endregion
    }
}
