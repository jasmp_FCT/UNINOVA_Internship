﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using AISCloud.Models.DAL;
using AISCloud.Common;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;

namespace AISCloud.Controllers
{
    public class GarageController : ApiController
    {
        private AISCloudModelContainer db = new AISCloudModelContainer();

        #region WebAPI

        #region RegisterGarage POST

        /// <summary>
        /// Register one Garage with Status Propertie
        /// </summary>
        /// <param name="Name">Garage Name</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.RegisterGarage)]
        [ResponseType(typeof(long))]
        public async Task<IHttpActionResult> PostRegisterGarage(string Name)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = db.Resource.Add(new Resource { Name = Name, ResourceTypesId = type.Id });

            db.Propertie.Add(new Propertie { ResourceId = garage.Id, Name = PropertieName.Status, Value = bool.TrueString });
            db.Propertie.Add(new Propertie { ResourceId = garage.Id, Name = PropertieName.Door, Value = bool.FalseString });
            db.Propertie.Add(new Propertie { ResourceId = garage.Id, Name = PropertieName.Requested, Value = bool.FalseString });

            if (await db.SaveChangesAsync() == 4) return Ok(garage.Id);
            return InternalServerError();
        }

        #endregion

        #region AssociateGarageToHome PUT

        /// <summary>
        /// Associate Garage to last registed House
        /// </summary>
        /// <param name="GarageID"> Garage ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.AssociateGarageToHome)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutAssociateGarageToHome(long GarageID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //check if is car and is register
            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = db.Resource.Find(GarageID);
            if (garage == null || type.Id != garage.ResourceTypesId) return BadRequest("Not a Garage!");

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = db.Resource.Where(b => b.ResourceTypesId == type.Id).OrderByDescending(x => x.Id).FirstOrDefault();

            if (house == null) return Ok(false);

            garage.ResourceId = house.Id;
            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return InternalServerError();
        }

        #endregion

        #region StatusOfGarage GET PUT

        /// <summary>
        /// Check Status of Garage
        /// </summary>
        /// <param name="GarageID">Garage ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.StatusOfGarage)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetStatusOfGarage(long GarageID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = await db.Resource.FindAsync(GarageID);
            if (garage == null || garage.ResourceTypesId != type.Id) return BadRequest("You Are Not a Garage");

            Propertie prop = db.Propertie.Where(x => x.ResourceId == GarageID && x.Name == PropertieName.Status).OrderByDescending(x => x.Id).FirstOrDefault();

            if (prop == null) return NotFound();
            return Ok(bool.Parse(prop.Value)); 
        }


        /// <summary>
        /// Change Status of Garage
        /// </summary>
        /// <param name="GarageID">Garage ID</param>
        /// <param name="value"> True if clear and False if occupied</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.StatusOfGarage)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutStatusOfGarage(long GarageID, bool value)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = await db.Resource.FindAsync(GarageID);
            if (garage == null || garage.ResourceTypesId != type.Id) return BadRequest("You Are Not a Garage");

            Propertie prop = db.Propertie.Where(x => x.ResourceId == GarageID && x.Name == PropertieName.Status).OrderByDescending(x => x.Id).FirstOrDefault();
            prop.Value = value.ToString();

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false); 
        }

        #endregion

        #region CheckFire
        /// <summary>
        /// Check if is Fire in the House
        /// </summary>
        /// <param name="GarageID">Garage ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.CheckFire)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetCheckFire(long GarageID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = await db.Resource.FindAsync(GarageID);
            if (garage == null || garage.ResourceTypesId != type.Id) return BadRequest("You Are Not a Garage");
            if (garage.ResourceId == null) return BadRequest("You are Not assigned to a House");

            Propertie prop = db.Propertie.Where(x => x.ResourceId == garage.ResourceId && x.Name == PropertieName.Fire).OrderByDescending(x => x.Id).FirstOrDefault();

            if (prop == null) return NotFound();
            return Ok(bool.Parse(prop.Value));
        }

        #endregion

        #region StatusDoor GET PUT
        /// <summary>
        /// Get Garage door status
        /// </summary>
        /// <param name="GarageID">Garage ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.GarageDoor)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetGarageDoor(long GarageID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = await db.Resource.FindAsync(GarageID);
            if (garage == null || garage.ResourceTypesId != type.Id) return BadRequest("You Are Not a Garage");

            Propertie prop = db.Propertie.Where(x => x.ResourceId == GarageID && x.Name == PropertieName.Door).OrderByDescending(x => x.Id).FirstOrDefault();

            if (prop == null) return NotFound();
            return Ok(bool.Parse(prop.Value));
        }

        /// <summary>
        /// Update Garage door status
        /// </summary>
        /// <param name="GarageID">Garage ID</param>
        /// <param name="value">False = close, True = open</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.GarageDoor)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutGarageDoor(long GarageID, bool value)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = await db.Resource.FindAsync(GarageID);
            if (garage == null || garage.ResourceTypesId != type.Id) return BadRequest("You Are Not a Garage");

            Propertie prop = db.Propertie.Where(x => x.ResourceId == GarageID && x.Name == PropertieName.Door).OrderByDescending(x => x.Id).FirstOrDefault();
            prop.Value = value.ToString();

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }

        #endregion

        #region Resquest GET PUT
        /// <summary>
        /// Check if Request 
        /// </summary>
        /// <param name="GarageID">Garage ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.Request)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetRequest(long GarageID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = await db.Resource.FindAsync(GarageID);
            if (garage == null || garage.ResourceTypesId != type.Id) return BadRequest("You Are Not a Garage");

            Propertie prop = db.Propertie.Where(x => x.ResourceId == GarageID && x.Name == PropertieName.Requested).OrderByDescending(x => x.Id).FirstOrDefault();

            if (prop == null) return NotFound();
            return Ok(bool.Parse(prop.Value));
        }

        /// <summary>
        /// Change Request
        /// </summary>
        /// <param name="GarageID">Garage IdD</param>
        /// <param name="value">Value = true or false</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.Request)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutRequest(long GarageID, bool value)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = await db.Resource.FindAsync(GarageID);
            if (garage == null || garage.ResourceTypesId != type.Id) return BadRequest("You Are Not a Garage");

            Propertie prop = db.Propertie.Where(x => x.ResourceId == GarageID && x.Name == PropertieName.Requested).OrderByDescending(x => x.Id).FirstOrDefault();
            prop.Value = value.ToString();

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }

        #endregion

        #region CreateAndAssociateSensor POST

        /// <summary>
        /// Create and associate a Sensor to the Garage
        /// </summary>
        /// <param name="GarageID"></param>
        /// <param name="SensorName"></param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.CreateAndAssociateSensor)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostCreateAndAssociateSensor(long GarageID, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType type = db.DeviceType.Where(b => b.Name == DevicesTypeName.SENSOR).FirstOrDefault();
            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource resource = db.Resource.Find(GarageID);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a Car or you are not register");

            Device sensor = db.Device.Add(new Device { Name = SensorName, DeviceTypeId = type.Id, ResourceId = GarageID });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region CreateAndAssociateActuator POST

        /// <summary>
        /// Create and associate a Actuator to the car
        /// </summary>
        /// <param name="GarageID"></param>
        /// <param name="SensorName"></param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.CreateAndAssociateActuator)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostCreateAndAssociateActuator(long GarageID, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType type = db.DeviceType.Where(b => b.Name == DevicesTypeName.ACTUADOR).FirstOrDefault();
            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource resource = db.Resource.Find(GarageID);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a Car or you are not register");

            Device sensor = db.Device.Add(new Device { Name = SensorName, DeviceTypeId = type.Id, ResourceId = GarageID });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region AddSensorReading POST

        /// <summary>
        /// Registe readings from a Garage sensor
        /// </summary>
        /// <param name="GarageID">Garage ID</param>
        /// <param name="SensorName">Sensor Name</param>
        /// <param name="DataName">Name of the reading propertie</param>
        /// <param name="Value">Value of the reading</param>
        /// <param name="TypeObject">Type of data of the reading</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.AddSensorReading)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostAddSensorReading(long GarageID, string SensorName, DataName DataName, string Value, TypeEnumName TypeObject)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource car = await db.Resource.FindAsync(GarageID);
            if (car == null || car.ResourceTypesId != type.Id) return BadRequest("You are not a car");

            DeviceType devType = db.DeviceType.Where(b => b.Name == DevicesTypeName.SENSOR).FirstOrDefault();
            Device dev = db.Device.Where(b => b.Name.ToLower() == SensorName.ToLower() && b.ResourceId == car.Id && b.DeviceTypeId == devType.Id).FirstOrDefault();

            if (dev == null) return BadRequest("Sensor don t exist or no associate o wrong type");

            db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = DataName, typeEnum = TypeObject, value = Value });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region AddActuatorAction POST

        /// <summary>
        /// Registe readings from a Garage Actuator
        /// </summary>
        /// <param name="GarageID">Garage ID</param>
        /// <param name="SensorName">Sensor Name</param>
        /// <param name="DataName">Name of the reading propertie</param>
        /// <param name="Value">Value of the reading</param>
        /// <param name="TypeObject">Type of data of the reading</param>
        /// <returns></returns>
        [ActionName(Dictionary.CarWebAPI.AddActuatorAction)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostAddActuatorAction(long GarageID, string SensorName, DataName DataName, string Value, TypeEnumName TypeObject)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource car = await db.Resource.FindAsync(GarageID);
            if (car == null || car.ResourceTypesId != type.Id) return BadRequest("You are not a car");

            DeviceType devType = db.DeviceType.Where(b => b.Name == DevicesTypeName.ACTUADOR).FirstOrDefault();
            Device dev = db.Device.Where(b => b.Name.ToLower() == SensorName.ToLower() && b.ResourceId == car.Id && b.DeviceTypeId == devType.Id).FirstOrDefault();

            if (dev == null) return BadRequest("Sensor don t exist or no associate o wrong type");

            db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = DataName, typeEnum = TypeObject, value = Value });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #endregion
    }
}
