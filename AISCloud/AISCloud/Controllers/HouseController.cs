﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AISCloud.Models.DAL;
using AISCloud.Common;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;

namespace AISCloud.Controllers
{
    public class HouseController : ApiController
    {
        private AISCloudModelContainer db = new AISCloudModelContainer();

        #region WebAPI

        #region RegisterHouse POST

        /// <summary>
        /// Register a house and 3 default properties(Max Temp, Min Temp, Request Status) in the cloud
        /// </summary>
        /// <param name="Name">House Name</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.RegisterHouse)]
        [ResponseType(typeof(long))]
        public async Task<IHttpActionResult> PostRegisterHouse(string Name)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = db.Resource.Add(new Resource { Name = Name, ResourceTypesId = type.Id });

            db.Propertie.Add(new Propertie { ResourceId = house.Id, Name = PropertieName.MinTemp, Value = "18" });
            db.Propertie.Add(new Propertie { ResourceId = house.Id, Name = PropertieName.MaxTemp, Value = "25" });
            db.Propertie.Add(new Propertie { ResourceId = house.Id, Name = PropertieName.Fire, Value = Boolean.FalseString });

            if (await db.SaveChangesAsync() == 4) return Ok(house.Id);
            return InternalServerError();
        }
        #endregion

        #region MaxTemperature PUT GET
        /// <summary>
        /// Get Max Temperature of one House
        /// </summary>
        /// <param name="HouseID">House Id</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.MaxTemperature)]
        [ResponseType(typeof(decimal))]
        public async Task<IHttpActionResult> GetMaxTemperature(long HouseID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = await db.Resource.FindAsync(HouseID);

            if (house == null || house.ResourceTypesId != type.Id) return BadRequest("You are Not a House!");

            Propertie maxtemp = db.Propertie.Where(b => b.ResourceId == house.Id && b.Name == PropertieName.MaxTemp).FirstOrDefault();
            if (maxtemp != null) Ok(decimal.Parse(maxtemp.Value));
            return InternalServerError();
        }


        /// <summary>
        /// Change Max Temperature of one House
        /// </summary>
        /// <param name="HouseID">House Id</param>
        /// <param name="value"> Decimal Temperature Value</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.MaxTemperature)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutMaxTemperature(long HouseID, decimal value)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = await db.Resource.FindAsync(HouseID);

            if (house == null || house.ResourceTypesId != type.Id) return BadRequest("You are Not a House!");

            Propertie maxtemp = db.Propertie.Where(b => b.ResourceId == house.Id && b.Name == PropertieName.MaxTemp).FirstOrDefault();
            if (maxtemp != null) maxtemp.Value = value.ToString();
            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }

        #endregion

        #region MinTemperature PUT GET

        /// <summary>
        /// Get Min Temperature of one House
        /// </summary>
        /// <param name="HouseID">House Id</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.MinTemperature)]
        [ResponseType(typeof(decimal))]
        public async Task<IHttpActionResult> GetMinTemperature(long HouseID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = await db.Resource.FindAsync(HouseID);

            if (house == null || house.ResourceTypesId != type.Id) return BadRequest("You are Not a House!");

            Propertie mintemp = db.Propertie.Where(b => b.ResourceId == house.Id && b.Name == PropertieName.MinTemp).FirstOrDefault();
            if (mintemp != null) Ok(decimal.Parse(mintemp.Value));
            return InternalServerError();
        }

        /// <summary>
        /// Change Min Temperature of one House
        /// </summary>
        /// <param name="HouseID">House Id</param>
        /// <param name="value"> Decimal Temperature Value</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.MinTemperature)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutMinTemperature(long HouseID, decimal value)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = await db.Resource.FindAsync(HouseID);

            if (house == null || house.ResourceTypesId != type.Id) return BadRequest("You are Not a House!");

            Propertie mintemp = db.Propertie.Where(b => b.ResourceId == house.Id && b.Name == PropertieName.MinTemp).FirstOrDefault();
            if (mintemp != null) mintemp.Value = value.ToString();
            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }

        #endregion

        #region Fire

        /// <summary>
        /// Get if is fire
        /// </summary>
        /// <param name="HouseID">House ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.Fire)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetFire(long HouseID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = await db.Resource.FindAsync(HouseID);

            if (house == null || house.ResourceTypesId != type.Id) return BadRequest("You are Not a House!");

            Propertie mintemp = db.Propertie.Where(b => b.ResourceId == house.Id && b.Name == PropertieName.Fire).FirstOrDefault();
            if (mintemp != null) return Ok(bool.Parse(mintemp.Value));
            return InternalServerError();
        }

        /// <summary>
        /// Set Fire
        /// </summary>
        /// <param name="HouseID">House ID</param>
        /// <param name="value">False or True</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.Fire)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutFire(long HouseID, bool value)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = await db.Resource.FindAsync(HouseID);

            if (house == null || house.ResourceTypesId != type.Id) return BadRequest("You are Not a House!");

            Propertie mintemp = db.Propertie.Where(b => b.ResourceId == house.Id && b.Name == PropertieName.Fire).FirstOrDefault();
            if (mintemp != null) mintemp.Value = value.ToString();
            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }

        #endregion

        #region SomeoneHome GET
        /// <summary>
        /// Get if someone is home need the garage
        /// </summary>
        /// <param name="HouseID">House ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.SomeOneInHome)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetSomeOneInHome(long HouseID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //check if is cooler and is register
            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = await db.Resource.FindAsync(HouseID);
            if (house == null || type.Id != house.ResourceTypesId) return BadRequest("Not a House!");

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = db.Resource.Where(b => b.ResourceId == house.Id && b.ResourceTypesId == type.Id).FirstOrDefault();
            if (garage == null) return BadRequest("Home don t have a garage!");
            Propertie prop = db.Propertie.Where(b => b.ResourceId == garage.Id && b.Name == PropertieName.Status).FirstOrDefault();

            if (prop != null) return Ok(!bool.Parse(prop.Value));
            return InternalServerError();
        }
        #endregion

        #region HaveGarage GET
        /// <summary>
        /// Check if house is associated to a garage
        /// </summary>
        /// <param name="HouseID">House ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.HaveGarage)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetHaveGarage(long HouseID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //check if is cooler and is register
            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = await db.Resource.FindAsync(HouseID);
            if (house == null || type.Id != house.ResourceTypesId) return BadRequest("Not a House!");

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = db.Resource.Where(b => b.ResourceId == house.Id && b.ResourceTypesId == type.Id).FirstOrDefault();
            if (garage == null) return Ok(false);
            return Ok(true);
        }
        #endregion

        #region CreateAndAssociateSensor POST

        /// <summary>
        /// Create and associate a Sensor to the House
        /// </summary>
        /// <param name="HouseID"></param>
        /// <param name="SensorName"></param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.CreateAndAssociateSensor)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostCreateAndAssociateSensor(long HouseID, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType type = db.DeviceType.Where(b => b.Name == DevicesTypeName.SENSOR).FirstOrDefault();
            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource resource = await db.Resource.FindAsync(HouseID);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a House or you are not register");

            Device sensor = db.Device.Add(new Device { Name = SensorName, DeviceTypeId = type.Id, ResourceId = HouseID });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region CreateAndAssociateActuator POST

        /// <summary>
        /// Create and associate a Actuator to the House
        /// </summary>
        /// <param name="HouseID"></param>
        /// <param name="SensorName"></param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.CreateAndAssociateActuator)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostCreateAndAssociateActuator(long HouseID, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType type = db.DeviceType.Where(b => b.Name == DevicesTypeName.ACTUADOR).FirstOrDefault();
            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource resource = db.Resource.Find(HouseID);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a House or you are not register");

            Device sensor = db.Device.Add(new Device { Name = SensorName, DeviceTypeId = type.Id, ResourceId = HouseID });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region AddSensorReading POST

        /// <summary>
        /// Registe readings from a House sensor
        /// </summary>
        /// <param name="HouseID">House ID</param>
        /// <param name="SensorName">Sensor Name</param>
        /// <param name="DataName">Name of the reading propertie</param>
        /// <param name="Value">Value of the reading</param>
        /// <param name="TypeObject">Type of data of the reading</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.AddSensorReading)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostAddSensorReading(long HouseID, string SensorName, DataName DataName, string Value, TypeEnumName TypeObject)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource car = await db.Resource.FindAsync(HouseID);
            if (car == null || car.ResourceTypesId != type.Id) return BadRequest("You are not a House");

            DeviceType devType = db.DeviceType.Where(b => b.Name == DevicesTypeName.SENSOR).FirstOrDefault();
            Device dev = db.Device.Where(b => b.Name.ToLower() == SensorName.ToLower() && b.ResourceId == car.Id && b.DeviceTypeId == devType.Id).FirstOrDefault();

            if (dev == null) return BadRequest("Sensor don t exist or no associate o wrong type");

            db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = DataName, typeEnum = TypeObject, value = Value });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region AddActuatorAction POST

        /// <summary>
        /// Registe readings from a House Actuator
        /// </summary>
        /// <param name="HouseID">House ID</param>
        /// <param name="SensorName">Sensor Name</param>
        /// <param name="DataName">Name of the reading propertie</param>
        /// <param name="Value">Value of the reading</param>
        /// <param name="TypeObject">Type of data of the reading</param>
        /// <returns></returns>
        [ActionName(Dictionary.HouseWebAPI.AddActuatorAction)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostAddActuatorAction(long HouseID, string SensorName, DataName DataName, string Value, TypeEnumName TypeObject)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource car = await db.Resource.FindAsync(HouseID);
            if (car == null || car.ResourceTypesId != type.Id) return BadRequest("You are not a House");

            DeviceType devType = db.DeviceType.Where(b => b.Name == DevicesTypeName.ACTUADOR).FirstOrDefault();
            Device dev = db.Device.Where(b => b.Name.ToLower() == SensorName.ToLower() && b.ResourceId == car.Id && b.DeviceTypeId == devType.Id).FirstOrDefault();

            if (dev == null) return BadRequest("Sensor don t exist or no associate o wrong type");

            db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = DataName, typeEnum = TypeObject, value = Value });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #endregion
    }
}
