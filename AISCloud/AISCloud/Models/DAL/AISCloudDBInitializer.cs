﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AISCloud.Models.DAL
{
    public class AISCloudDBInitializer : DropCreateDatabaseAlways<AISCloudModelContainer>
    {
        protected override void Seed(AISCloudModelContainer context)
        {
            context.DeviceType.Add(new DeviceType { Name = DevicesTypeName.SENSOR });
            context.DeviceType.Add(new DeviceType { Name = DevicesTypeName.ACTUADOR });


            context.ResourcesType.Add(new ResourcesType { Name = ResourcesTypeName.CAR });
            context.ResourcesType.Add(new ResourcesType { Name = ResourcesTypeName.COOLER });
            context.ResourcesType.Add(new ResourcesType { Name = ResourcesTypeName.FREEZER });
            context.ResourcesType.Add(new ResourcesType { Name = ResourcesTypeName.GARAGE });
            context.ResourcesType.Add(new ResourcesType { Name = ResourcesTypeName.HOME });

        }
    }
}