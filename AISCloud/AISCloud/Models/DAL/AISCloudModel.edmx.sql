
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/01/2014 18:23:01
-- Generated from EDMX file: C:\Users\GRIS\Desktop\AIS FINAL Apresentação\AISFinal\AISCloud\AISCloud\Models\DAL\AISCloudModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [AISdb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ResourcesResourcesTypes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Resource] DROP CONSTRAINT [FK_ResourcesResourcesTypes];
GO
IF OBJECT_ID(N'[dbo].[FK_DeviceDeviceType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Device] DROP CONSTRAINT [FK_DeviceDeviceType];
GO
IF OBJECT_ID(N'[dbo].[FK_DevicePropertie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Propertie] DROP CONSTRAINT [FK_DevicePropertie];
GO
IF OBJECT_ID(N'[dbo].[FK_DeviceService]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Service] DROP CONSTRAINT [FK_DeviceService];
GO
IF OBJECT_ID(N'[dbo].[FK_ResourceDevice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Device] DROP CONSTRAINT [FK_ResourceDevice];
GO
IF OBJECT_ID(N'[dbo].[FK_DeviceData]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Data] DROP CONSTRAINT [FK_DeviceData];
GO
IF OBJECT_ID(N'[dbo].[FK_ResourcePropertie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Propertie] DROP CONSTRAINT [FK_ResourcePropertie];
GO
IF OBJECT_ID(N'[dbo].[FK_ResourceResource]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Resource] DROP CONSTRAINT [FK_ResourceResource];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Device]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Device];
GO
IF OBJECT_ID(N'[dbo].[DeviceType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DeviceType];
GO
IF OBJECT_ID(N'[dbo].[Resource]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Resource];
GO
IF OBJECT_ID(N'[dbo].[Service]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Service];
GO
IF OBJECT_ID(N'[dbo].[Propertie]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Propertie];
GO
IF OBJECT_ID(N'[dbo].[ResourcesType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ResourcesType];
GO
IF OBJECT_ID(N'[dbo].[Data]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Data];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Device'
CREATE TABLE [dbo].[Device] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [DeviceTypeId] int  NOT NULL,
    [ResourceId] bigint  NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DeviceType'
CREATE TABLE [dbo].[DeviceType] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] int  NOT NULL
);
GO

-- Creating table 'Resource'
CREATE TABLE [dbo].[Resource] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [ResourceTypesId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ResourceId] bigint  NULL
);
GO

-- Creating table 'Service'
CREATE TABLE [dbo].[Service] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [DeviceId] bigint  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Propertie'
CREATE TABLE [dbo].[Propertie] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [DeviceId] bigint  NULL,
    [Name] smallint  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [ResourceId] bigint  NULL
);
GO

-- Creating table 'ResourcesType'
CREATE TABLE [dbo].[ResourcesType] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] int  NOT NULL
);
GO

-- Creating table 'Data'
CREATE TABLE [dbo].[Data] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [NameProp] smallint  NOT NULL,
    [value] nvarchar(max)  NOT NULL,
    [Date] datetime  NOT NULL,
    [DeviceId] bigint  NOT NULL,
    [typeEnum] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Device'
ALTER TABLE [dbo].[Device]
ADD CONSTRAINT [PK_Device]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DeviceType'
ALTER TABLE [dbo].[DeviceType]
ADD CONSTRAINT [PK_DeviceType]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Resource'
ALTER TABLE [dbo].[Resource]
ADD CONSTRAINT [PK_Resource]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Service'
ALTER TABLE [dbo].[Service]
ADD CONSTRAINT [PK_Service]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Propertie'
ALTER TABLE [dbo].[Propertie]
ADD CONSTRAINT [PK_Propertie]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ResourcesType'
ALTER TABLE [dbo].[ResourcesType]
ADD CONSTRAINT [PK_ResourcesType]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Data'
ALTER TABLE [dbo].[Data]
ADD CONSTRAINT [PK_Data]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ResourceTypesId] in table 'Resource'
ALTER TABLE [dbo].[Resource]
ADD CONSTRAINT [FK_ResourcesResourcesTypes]
    FOREIGN KEY ([ResourceTypesId])
    REFERENCES [dbo].[ResourcesType]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ResourcesResourcesTypes'
CREATE INDEX [IX_FK_ResourcesResourcesTypes]
ON [dbo].[Resource]
    ([ResourceTypesId]);
GO

-- Creating foreign key on [DeviceTypeId] in table 'Device'
ALTER TABLE [dbo].[Device]
ADD CONSTRAINT [FK_DeviceDeviceType]
    FOREIGN KEY ([DeviceTypeId])
    REFERENCES [dbo].[DeviceType]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DeviceDeviceType'
CREATE INDEX [IX_FK_DeviceDeviceType]
ON [dbo].[Device]
    ([DeviceTypeId]);
GO

-- Creating foreign key on [DeviceId] in table 'Propertie'
ALTER TABLE [dbo].[Propertie]
ADD CONSTRAINT [FK_DevicePropertie]
    FOREIGN KEY ([DeviceId])
    REFERENCES [dbo].[Device]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DevicePropertie'
CREATE INDEX [IX_FK_DevicePropertie]
ON [dbo].[Propertie]
    ([DeviceId]);
GO

-- Creating foreign key on [DeviceId] in table 'Service'
ALTER TABLE [dbo].[Service]
ADD CONSTRAINT [FK_DeviceService]
    FOREIGN KEY ([DeviceId])
    REFERENCES [dbo].[Device]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DeviceService'
CREATE INDEX [IX_FK_DeviceService]
ON [dbo].[Service]
    ([DeviceId]);
GO

-- Creating foreign key on [ResourceId] in table 'Device'
ALTER TABLE [dbo].[Device]
ADD CONSTRAINT [FK_ResourceDevice]
    FOREIGN KEY ([ResourceId])
    REFERENCES [dbo].[Resource]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ResourceDevice'
CREATE INDEX [IX_FK_ResourceDevice]
ON [dbo].[Device]
    ([ResourceId]);
GO

-- Creating foreign key on [DeviceId] in table 'Data'
ALTER TABLE [dbo].[Data]
ADD CONSTRAINT [FK_DeviceData]
    FOREIGN KEY ([DeviceId])
    REFERENCES [dbo].[Device]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DeviceData'
CREATE INDEX [IX_FK_DeviceData]
ON [dbo].[Data]
    ([DeviceId]);
GO

-- Creating foreign key on [ResourceId] in table 'Propertie'
ALTER TABLE [dbo].[Propertie]
ADD CONSTRAINT [FK_ResourcePropertie]
    FOREIGN KEY ([ResourceId])
    REFERENCES [dbo].[Resource]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ResourcePropertie'
CREATE INDEX [IX_FK_ResourcePropertie]
ON [dbo].[Propertie]
    ([ResourceId]);
GO

-- Creating foreign key on [ResourceId] in table 'Resource'
ALTER TABLE [dbo].[Resource]
ADD CONSTRAINT [FK_ResourceResource]
    FOREIGN KEY ([ResourceId])
    REFERENCES [dbo].[Resource]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ResourceResource'
CREATE INDEX [IX_FK_ResourceResource]
ON [dbo].[Resource]
    ([ResourceId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------