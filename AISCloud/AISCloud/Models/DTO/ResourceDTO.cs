﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AISCloud.Models.DTO
{
    public class ResourceDTO
    {
        public string Name { get; set; }
        public bool value { get; set; }
        public DateTime data { get; set; }
    }
}